import random
from locust import HttpLocust, TaskSet, task

from config import API_LOCATION


class BacktestBehavior(TaskSet):
    def __init__(self, parent):
        super().__init__(parent)
        self._supported_strategies = ['monthly', 'yearly', 'daily']

    @task(3)
    def get_catalog_global(self):
        self.client.get("/catalog/global")

    @task(3)
    def get_strategies(self):
        self.client.get("/backtest/strategies/")

    def backtest_with_strategy(self):
        self.client.post("/backtest/" + random.choice(self._supported_strategies))

    def filter_coins(self):
        self.client.post("/backtest/filter/")


class BacktestLocust(HttpLocust):
    host = API_LOCATION
    task_set = BacktestBehavior
    min_wait = 1000
    max_wait = 1000
