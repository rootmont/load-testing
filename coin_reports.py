import requests
import random
from locust import HttpLocust, TaskSet, task
from typing import List

from config import API_LOCATION


class CoinReportBehavior(TaskSet):
    __all_coins: List[str] = []

    def __init__(self, parent):
        super().__init__(parent)
        if len(self.__all_coins) == 0:
            req = requests.get(API_LOCATION + "/all-coins")
            self.__all_coins = [c.get("ticker_symbol") for c in req.json()]

    @task(1)
    def daily_stats(self):
        self.client.get("/daily-stats")

    @task(5)
    def get_metcalfe_chart_data(self):
        symbol = random.choice(self.__all_coins)
        self.client.get("/metcalfe-charts/" + symbol)

    @task(5)
    def get_price_chart_data(self):
        symbol = random.choice(self.__all_coins)
        self.client.get("/price-charts/" + symbol)

    @task(5)
    def get_coin_report(self):
        symbol = random.choice(self.__all_coins)
        self.client.get("/coin-report/" + symbol)

    @task(3)
    def disclosure(self):
        self.client.get("/all-disclosure")


class CoinReportLocust(HttpLocust):
    host = API_LOCATION
    task_set = CoinReportBehavior
    min_wait = 1000
    max_wait = 1000
