import requests
import random
import json
from locust import HttpLocust, TaskSet, task
from typing import List

from config import API_LOCATION


class DashboardBehavior(TaskSet):
    __all_coins: List[str] = []
    __all_master_table_cols: List[str] = []
    __all_master_table_rows: List[str] = []

    def __init__(self, parent):
        super().__init__(parent)
        if len(self.__all_coins) == 0:
            req = requests.get(API_LOCATION + "/all-coins")
            self.__all_coins = [c.get("ticker_symbol") for c in req.json()]

        if len(self.__all_master_table_cols) == 0:
            req = requests.get(API_LOCATION + "/master-table/columns")
            self.__all_master_table_cols = req.json()

    def get_master_table(self):
        rows = self.get_random_master_rows()
        cols = self.get_random_master_cols()
        self.client.get("/master-table/?rows=[" + ",".join(["\"" + r + "\"" for r in rows]) +
                        "]&cols=[" + ",".join(["\"" + c + "\"" for c in cols]) + "]")

    @task(1)
    def get_master_table_all(self):
        self.client.get("/master-table/all")

    @task(1)
    def get_master_table_columns(self):
        self.client.get("/master-table/columns")

    @task(1)
    def get_ntx(self):
        self.client.get("/ntx/" + self.get_random_coin_query())  # TODO: check for 0s and NaN

    @task(1)
    def get_vol(self):
        self.client.get("/vol/" + self.get_random_coin_query())  # TODO: check for 0s and NaN

    @task(1)
    def get_mcap(self):
        self.client.get("/mcap/" + self.get_random_coin_query())  # TODO: check for 0s and NaN

    @task(1)
    def get_price(self):
        self.client.get("/price/" + self.get_random_coin_query())  # TODO: check for 0s and NaN

    @task(1)
    def get_price_open(self):
        self.client.get("/price/open/" + self.get_random_coin_query())  # TODO: check for 0s and NaN

    @task(1)
    def get_supply(self):
        self.client.get("/supply/" + self.get_random_coin_query())  # TODO: check for 0s and NaN

    @task(1)
    def get_movers(self):
        days_back: int = random.randrange(1, 700)
        num_movers: int = random.randrange(1, 20)
        self.client.get("/movers/" + str(days_back) + "/" + str(num_movers))

    def get_random_coin_query(self) -> str:
        params: List[str] = []
        num_params = random.randrange(1, 20)
        for i in range(0, num_params):
            params.append(random.choice(self.__all_coins))
        return "?symbols=" + ",".join(params)

    def get_random_master_cols(self) -> List[str]:
        params: List[str] = []
        num_params = random.randrange(2, len(self.__all_master_table_cols))
        for i in range(0, num_params):
            params.append(random.choice(self.__all_master_table_cols))
        return params

    def get_random_master_rows(self) -> List[str]:
        params: List[str] = []
        num_params = random.randrange(2, len(self.__all_coins))
        for i in range(0, num_params):
            params.append(random.choice(self.__all_coins))
        return params


class DashboardLocust(HttpLocust):
    host = API_LOCATION
    task_set = DashboardBehavior
    min_wait = 1000
    max_wait = 1000
