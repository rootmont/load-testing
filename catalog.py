import random
import requests
from locust import HttpLocust, TaskSet, task
from typing import List

from config import API_LOCATION


class CatalogBehavior(TaskSet):
    __all_ages: List[str] = []
    __all_industries: List[str] = []
    __all_marketcaps: List[str] = []

    def __init__(self, parent):
        super().__init__(parent)
        if len(self.__all_ages) == 0 or len(self.__all_marketcaps) == 0 or len(self.__all_industries) == 0:
            req = requests.get(API_LOCATION + "/all-categories")
            j = req.json()
            self.__all_ages = j["ages"]
            self.__all_industries = j["industries"]
            self.__all_marketcaps = j["marketcaps"]

    @task(3)
    def get_catalog_global(self):
        self.client.get("/catalog/global")

    @task(5)
    def get_catalog_industry(self):
        industry = random.choice(self.__all_industries)
        self.client.get("/catalog/industry/" + industry)

    @task(5)
    def get_catalog_marketcap(self):
        market_cap = random.choice(self.__all_marketcaps)
        self.client.get("/catalog/marketcap/" + market_cap)

    @task(5)
    def get_catalog_age(self):
        age = random.choice(self.__all_ages)
        self.client.get("/catalog/age/" + age)

    @task(5)
    def get_all_coins(self):
        self.client.get("/all-coins")


class CatalogLocust(HttpLocust):
    host = API_LOCATION
    task_set = CatalogBehavior
    min_wait = 1000
    max_wait = 1000
