from locust import HttpLocust, TaskSet, task

from config import API_LOCATION


class BenchmarkBehavior(TaskSet):
    def __init__(self, parent):
        super().__init__(parent)

    @task(3)
    def get_benchmark_marketcap(self):
        self.client.get("/benchmark/marketcap")

    @task(3)
    def get_benchmark_age(self):
        self.client.get("/benchmark/age")

    @task(3)
    def get_benchmark_industry(self):
        self.client.get("/benchmark/industry")


class BenchmarkLocust(HttpLocust):
    host = API_LOCATION
    task_set = BenchmarkBehavior
    min_wait = 1000
    max_wait = 1000
