Load Testing
---

Using locust - [https://locust.io/](https://locust.io/).

View the documentation surrounding the CLI version of locust. In the docker-compose file there are two flags; -r and -c. -r represents the number of users to "hatch" per second (name of the lib is locust lol.) and -c is the total number of users to start up to test.

Locust is an interesting library because you can bring up other slave servers to add more stress if one machine is not good enough. Nice tool to learn for Defcon.

### Requirements
* Docker 
* docker-compose

### Running

Run 2 min test against daily-coin endpoints
```
docker-compose up daily-coin
```

Run 2 min test against catalog endpoints
```
docker-compose up catalog
```

Run 2 min test against backtest endpoints
```
docker-compose up backtest
```

Run 2 min test against benchmark endpoints
```
docker-compose up benchmark
```

Run 2 min test against dashboard endpoints (currently broken)
```
docker-compose up dashboard
```

### Load Testing Functions that still need love:
#### backtest.py;
Not sure what exactly todo with these post requests (the filter)
```
    def backtest_with_strategy(self):
        self.client.post("/backtest/" + random.choice(self._supported_strategies))

    def filter_coins(self):
        self.client.post("/backtest/filter/")
```

#### dashboard.py
Not sure how to exactly get this request right with the query params
```
    def get_master_table(self):
        rows = self.get_random_master_rows()
        cols = self.get_random_master_cols()
        self.client.get("/master-table/?rows=[" + ",".join(["\"" + r + "\"" for r in rows]) +
                        "]&cols=[" + ",".join(["\"" + c + "\"" for c in cols]) + "]")
```

